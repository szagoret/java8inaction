package java8inaction.ch11;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author szagoret
 */
public class FutureInterface {

    public static Double doSomethingLongComputation() {
        System.out.println("Start Do something long computation");
        for (int i = 0; i < 1000; i++) {
            int[] arr = new int[10000];
            for (int j = 0; j < 1000; j++) {
                arr[j] = i * j + 2;
            }

        }
        System.out.println("End Do something long computation");
        return Double.MIN_NORMAL;
    }

    public static void doSomethingElse() {
        System.out.println("Do something else");
    }

    public static void main(String[] args) {

        /**
         * Executing a long-lasting operation asynchronously before Java 8
         */
        // Create an ExecutorService allowing you to submit tasks to a thread pool.
        ExecutorService executor = Executors.newCachedThreadPool();

        Future<Double> future = executor.submit(new Callable<Double>() {

            @Override
            public Double call() throws Exception {
                // Execute a long operation asynchronously in a separate thread
                return doSomethingLongComputation();
            }
        });
        // Do something else while the asyncronous operation is progressing
        doSomethingElse();

        //Retreive the result of the asynchronous operation, eventyaly blocking if it isn't
        // available yet, but waiting at most for 1 second
        try {
            Double result = future.get(1, TimeUnit.SECONDS);
        } catch (ExecutionException executionException) {
            // the computation threw an exception
        } catch (InterruptedException interruptedException) {
            // the current thread wsa interrupted while waiting
        } catch (TimeoutException timeoutException) {
            // the timeout expired before the Future completion
        }
    }
}
