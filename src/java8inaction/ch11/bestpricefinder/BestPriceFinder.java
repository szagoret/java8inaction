package java8inaction.ch11.bestpricefinder;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author szagoret
 */
public class BestPriceFinder {

    public static void main(String[] args) {
        Shop shop = new Shop("BestShop");
        long start = System.nanoTime();
        // Query the shop to retreive the price of a product
        Future<Double> futurePrice = shop.getPriceAsyncFactory("my favorite product");
        long invocationTime = ((System.nanoTime() - start) / 1000000);
        System.out.println("Invocation returned after " + invocationTime + " msec");

        // DO some more tasks, like quering other shops
        doSomethingElse();

        // while the price of the product is being calculated
        try {
            // Read the price from the Future or block until it won't be available
            double price = futurePrice.get();
            System.out.printf("Price is %.2f%n", price);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        long retrievalTime = ((System.nanoTime() - start) / 1000000);
        System.out.println("Price returned after " + retrievalTime + " msec");

        /**
         * Make code non-blocking
         */
        List<Shop> shops = Arrays.asList(new Shop("BestPrice"), new Shop("LetsSaveBig"),
                new Shop("MyFavoriteShop"), new Shop("BuyItAll"), new Shop("MyFirst"), new Shop("MySecond"),
                new Shop("MyThird"));
        // Return a list og strings, where each string contains the name of a shop and the price
        // of requested product in that shop

        // cheking correctenn and preformance
        start = System.nanoTime();
        System.out.println(findPrices(shops, "myPhone27S"));
        long duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in " + duration + " msec");

        //create a thread pool with a number of threads equals to the minimum
        // between 100 and the number of shops
        final Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100),
                (Runnable r) -> {
                    Thread t = new Thread(r);
                    // Use daemon threads-they don't prevent
                    // the termination of the program
                    t.setDaemon(true);
                    return t;
                });
        // cheking correctenn and preformance
        start = System.nanoTime();
        System.out.println(findPrices(shops, "myPhone27S"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Executor is Done in " + duration + " msec");
        // find prices sync
        start = System.nanoTime();
        System.out.println(findPricesSync(shops, "myPhone27S"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Find prices sync:  " + duration + " msec");
        // find prices async
        start = System.nanoTime();
        System.out.println(findPricesAsync(shops, "myPhone27S", executor));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Find prices async:  " + duration + " msec");
        //        Future<Double> futurePriceInUSD = CompletableFuture.supplyAsync(() -> shop.getPrice("BestShop"))
        //                .thenCombine(CompletableFuture.supplyAsync(() -> exchangeService.getRate(Money.EUR, Money.USD)), (price, rate) -> price * rate);
        //findPricesStream(shops, "myPhone", executor).map(f -> f.thenAccept(System.out::println));
        final long threadStart = System.nanoTime();
        CompletableFuture[] futures = findPricesStream(shops, "myPhone", executor).map(f
                -> f.thenAccept(
                        s -> System.out.println(s + "(done in )"
                                + ((System.nanoTime() - threadStart) / 1000000) + " msec")))
                .toArray(size -> new CompletableFuture[size]);

        /**
         * <code>ollOf</code> factory method takes as input an array of CompletableFutures and
         * returns CompletableFuture<Void> that's completed only when all the COmpletableFutures
         * passed have completed. This means that invoking join on the CompletableFuture returned by
         * <code>allOF</code> method provides an easy way to wait for the completion of all the
         * COmpletableFutures in the original stream
         */
        CompletableFuture.allOf(futures).join();
        System.out.println("All shops have now responded in"
                + ((System.nanoTime() - threadStart) / 1000000) + " msec");
    }

    public static Stream<CompletableFuture<String>> findPricesStream(List<Shop> shops, String product, Executor executor) {
        return shops.stream().map(shop -> CompletableFuture.supplyAsync(() -> shop.getPrice(product), executor))
                .map(future -> future.thenApply(Quote::parse))
                .map(future -> future.thenCompose(quote
                                -> CompletableFuture.supplyAsync(() -> Discount.applyDicount(quote), executor)));
    }

    public static List<String> findPrices(List<Shop> shops, String product) {
        List<CompletableFuture<String>> pricesFutures = shops.stream().map(shop -> CompletableFuture.supplyAsync(() -> String.format("%s price is %s", shop.getName(),
                shop.getPrice(product)))).collect(Collectors.toList());
        return pricesFutures.stream().map(CompletableFuture::join).collect(Collectors.toList());
    }

    public static List<String> findPricesSync(List<Shop> shops, String product) {
        // Retreive the nondiscounted price from each shop
        //Transform each shop
        // Transform the Strings returned by the shops in Quote objects
        // COntact the Dicount service to apply the discount on each Quote
        return shops.stream()
                .map(shop -> shop.getPrice(product))
                .map(Quote::parse)
                .map(Discount::applyDicount)
                .collect(Collectors.toList());
    }

    public static List<String> findPricesAsync(List<Shop> shops, String product, Executor executor) {
        // Asynchronously retreive the nondiscounted price from each shop
        List<CompletableFuture<String>> priceFutures = shops.stream()
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getPrice(product), executor))
                .peek((CompletableFuture t) -> System.out.println("(1): " + t.toString()))
                // Transform the String returned by a shop into a Quote object when it becomes available
                .map(future -> future.thenApply(Quote::parse)).peek((CompletableFuture t) -> System.out.println("(2): " + t.toString()))
                // Wait to be completed and extract their respective results
                .map(future -> future.thenCompose(
                                quote -> CompletableFuture.supplyAsync(
                                        () -> Discount.applyDicount(quote), executor))).peek((CompletableFuture t) -> System.out.println("(3): " + t.toString()))
                .collect(Collectors.toList());

        return priceFutures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static void doSomethingElse() {
        System.out.println("Do something else");
    }
}
