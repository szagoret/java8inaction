package java8inaction.ch11.bestpricefinder;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

/**
 *
 * @author szagoret
 */
public class Shop {

    String name;

    Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Future<Double> getPriceAsyncFactory(String product) {
        return CompletableFuture.supplyAsync(() -> calculatePrice(product));
    }

    public Future<Double> getPriceAsync(String product) {
        CompletableFuture<Double> futurePrice = new CompletableFuture<>();
        new Thread(() -> {
            try {
                double price = calculatePrice(product);
                // Set the value returned by the long computation on the Future
                // when it becomes available
                // If the price calculation completed normally, complete the Future with the price
                futurePrice.complete(price);
            } catch (Exception ex) {
                // Otherwise, complete it exceptionlly with the Exception that caused the faulure
                futurePrice.completeExceptionally(ex);
            }
        }).start();
        // Return the Future without waiting for the computation of the result it contains
        // to be completed
        return futurePrice;
    }

    public String getPrice(String product) {
        double price = calculatePrice(product);
        Random random = new Random();
        Discount.Code code = Discount.Code.values()[random.nextInt(Discount.Code.values().length)];
        return String.format("%s:%.2f:%s", name, price, code);
    }

    private double calculatePrice(String product) {
        delay();
        // return a randomly calculated value
//        throw new NumberFormatException();
        Random random = new Random();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }

    private static final Random random = new Random();

    public static final void randomDelay() {
        int delay = 500 + random.nextInt(2000);
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ie) {
            throw new RuntimeException(ie);
        }
    }

    /**
     * Simulate long-running method execution
     */
    public static void delay() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }

}
