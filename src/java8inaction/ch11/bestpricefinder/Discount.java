package java8inaction.ch11.bestpricefinder;

/**
 *
 * @author szagoret
 */
public class Discount {

    public enum Code {

        NONE(0), SILVER(5), GOLD(10), PLATINUM(15), DIAMOND(20);

        private final int percentage;

        private Code(int percentage) {
            this.percentage = percentage;
        }

    }

    public static String applyDicount(Quote quote) {
        // Apply the dicount code to the original
        return quote.getShopName() + " price is " + Discount.apply(quote.getPrice(), quote.getDiscountCode());
    }

    private static String apply(double price, Code discountCode) {
        Shop.delay();
        return format(price * (100 - discountCode.percentage));
    }

    private static String format(double d) {
        return String.format("Best doscount price is: %.2f", d);
    }
}
