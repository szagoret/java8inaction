package java8inaction.ch11.bestpricefinder;

import java.text.NumberFormat;
import java.text.ParseException;

/**
 *
 * @author szagoret
 */
public class Quote {

    private final String shopName;
    private final double price;
    private final Discount.Code discountCode;

    private Quote(String shopName, double price, Discount.Code discountCode) {
        this.shopName = shopName;
        this.price = price;
        this.discountCode = discountCode;
    }

    public static Quote parse(String s) {
        String[] split = s.split(":");
        String shopName = split[0];
        NumberFormat format = NumberFormat.getInstance();
        Number number;
        try {
            number = format.parse(split[1]);
        } catch (ParseException exception) {
            throw new RuntimeException(exception);
        }
        double price = number.floatValue();
        //double price = Double.parseDouble(split[1]);
        Discount.Code discountCode = Discount.Code.valueOf(split[2]);
        return new Quote(shopName, price, discountCode);
    }

    public String getShopName() {
        return shopName;
    }

    public double getPrice() {
        return price;
    }

    public Discount.Code getDiscountCode() {
        return discountCode;
    }

}
