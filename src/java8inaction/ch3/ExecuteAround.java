package java8inaction.ch3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import static java.util.Comparator.comparing;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java8inaction.ch2.FilteringApples.Apple;
import java8inaction.ch2.FilteringApples.AppleColor;

public class ExecuteAround {

    public static void main(String[] args) throws IOException {
        List<Manager> managers = Arrays.asList(
                new Manager("Vasea", 20, "Lead"), new Manager("Tolea", 24,
                        "Prod"), new Manager("Vitea", 34, "Test"), new Manager(
                        "Maria", 23, "QA"));

        // process one line
        String oneLine = processFile((BufferedReader br) -> br.readLine());
        System.out.println(oneLine);

        // process two lines
        String twoLines = processFile((BufferedReader br) -> {
            return br.readLine() + ", " + br.readLine();
        });
        System.out.println(twoLines);

        forEach(Arrays.asList(1, 2, 3, 4, 5),
                (Integer i) -> System.out.println("Element:" + i));

        List<Integer> intStrings = map(
                Arrays.asList("Mar", "Prasada", "Covrigi"), (String s) -> {
                    return s.length();
                });
        System.out.println(intStrings);
        List<Integer> mT = Arrays.asList("Mar", "Prasada", "Covrigi").stream().mapToInt((s) -> s.length()).boxed().collect(Collectors.toList());
        System.out.println("MT-" + mT);

        Employee employee = produceElementOfPerson(Employee::new);
        System.out.println(employee);

        Manager manager = produceElementOfPerson(() -> new Manager("Kolea", 1,
                "Vas"));
        System.out.println(manager);

        System.out.println("Managers before sort:\n" + managers);
        // sort a collection using method reference
        managers.sort(Comparator.comparing(Manager::getPosition));
        System.out.println("Managers after sort:\n" + managers);

        // sort a list using method reference
        List<String> str = Arrays.asList("a", "b", "A", "B");
        str.sort(String::compareToIgnoreCase);
        System.out.println(str);

        /* constructor reference */
        // constructor with arguments
        Function<String, Manager> cFunction = Manager::new;
        Manager m1 = cFunction.apply("Manager");
        System.out.println(m1);

        // constructor with two arguments
        BiFunction<String, Integer, Person> bf1 = Person::new;
        BiFunction<String, Integer, Person> bf2 = (name, age) -> new Person(
                name, age);
        Person p1 = bf1.apply("Anatol", 15);
        Person p2 = bf2.apply("Vasea", 20);
        System.out.println("Two arguments constr:\n" + p1 + "\n" + p2);

        Person p3 = giveMePerson("person", 20);
        Person p4 = giveMePerson("person", 50);
        System.out.println("Give me a person:" + p3 + "\n" + p4);

        // constr with two params
        Person p5 = giveMePersonWithParams("person", 22);
        Person p6 = giveMePersonWithParams("empolyee", 20);
        System.out.println("Give me a person with params:\n" + p5 + "\n" + p6);

        // comparator forms
        // sort reverse
        List<Apple> inventory = Arrays.asList(new Apple(20, AppleColor.red),
                new Apple(20, AppleColor.blue),
                new Apple(200, AppleColor.green),
                new Apple(155, AppleColor.red), new Apple(120, "green"));
        inventory.sort(comparing(Apple::getWeight).reversed());
        System.out.println("Reversed sort by weight:" + inventory);

        // if we have two apples with same weight, we need to compare by color,
        // for that we use a second comparator
        inventory.sort(comparing(Apple::getWeight).reversed().thenComparing(
                Apple::getColor));
        System.out.println("Reverse sort by weight and color:" + inventory);

        Predicate<Apple> redGreen = (a) -> "red".equals(a.getColor())
                || "green".equals(a.getColor());
        List<Apple> redApplesList = abstractFilter(inventory, redGreen);
        System.out.println("Red Green Apples List" + redApplesList);

        // composing predicate
        Predicate<Apple> redGreenApples = (apple) -> AppleColor.red.name()
                .equals(apple.getColor())
                || AppleColor.green.name().equals(apple.getColor());

        Predicate<Apple> redGreanApplesGreatherThan50 = redGreenApples
                .and(apple -> apple.getWeight() > 50);

        Predicate<Apple> redGreanApplesGreatherThan50OrBlue = redGreanApplesGreatherThan50
                .or(a -> AppleColor.blue.name().equals(a.getColor()));

        List<Apple> redGreanApplesGreatherThan50OrBlueList = abstractFilter(
                inventory, redGreanApplesGreatherThan50OrBlue);
        System.out
                .println("Read green apples and greather than 50 or blue List"
                        + redGreanApplesGreatherThan50OrBlueList);

        // composing functions
        Function<Integer, Integer> f = x -> x + 1;
        Function<Integer, Integer> g = x -> x * 2;
        Function<Integer, Integer> h = f.andThen(g);
        Function<Integer, Integer> hCompose = f.compose(g);
        int resultAndThen = h.apply(1);
        System.out.println("Function result:" + resultAndThen);
        int resultCompose = hCompose.apply(1);
        System.out.println("Function result:" + resultCompose);

        // create transformation pipeline using Function
        Function<String, String> addHeader = Letter::addHeader;
        Function<String, String> transformationPipeline = addHeader.andThen(
                Letter::checkSpelling).andThen(Letter::addFooter);

        // argument [OK labda] send to the function will be used from all
        // functions
        String letter = transformationPipeline.apply("OK labda");
        System.out.println("Letter :" + letter);

        /**
         * Rezolvarea ecuatiilor de gradul II
         *
         */
        TriFunction<Integer, Integer, Integer, Double> discriminant = (a, b, c) -> Math.pow(b.doubleValue(), 2)
                - 4 * a * c;

        TriFunction<Integer, Integer, Integer, Double> positiveDiscriminantS1 = (a, b, c) -> (-b
                - Math.sqrt(c) / 2 * a);

        TriFunction<Integer, Integer, Integer, Double> positiveDiscriminantS2 = (a, b, c) -> (-b
                + Math.sqrt(c) / 2 * a);

        TriFunction<Integer, Integer, Integer, Double> zeroDiscriminantS12 = (a, b, c) -> Double.valueOf(-(b / 2 * a));

        TriFunction<Integer, Integer, Integer, Double> positiveDiscriminantS1Result = discriminant
                .andThen(positiveDiscriminantS1);
        TriFunction<Integer, Integer, Integer, Double> positiveDiscriminantS2Result = discriminant
                .andThen(positiveDiscriminantS2);
        TriFunction<Integer, Integer, Integer, Double> zeroDiscriminantS12Result = discriminant
                .andThen(zeroDiscriminantS12);

        Double fDResult = discriminant.apply(1, 4, 4);
        System.out.println("Discriminant of: 6x^2+5x+1=0 : " + fDResult);
        if (fDResult > 0) {
            System.out.println("S1 = " + positiveDiscriminantS1Result.apply(1, 4, 4));
            System.out.println("S2 = " + positiveDiscriminantS2Result.apply(1, 4, 4));
        } else if (fDResult == 0) {
            System.out.println("S1 = S2 = " + zeroDiscriminantS12Result.apply(1, 4, 4));
        }
    }

    public static class Letter {

        public static String addHeader(String text) {
            return "From:" + text;
        }

        public static String addFooter(String text) {
            return text + ", Best Regards";
        }

        public static String checkSpelling(String text) {
            return text.replaceAll("labda", "lambda");
        }
    }

    // using Function with Map to associate constructors with String value
    static Map<String, Function<Integer, Person>> map = new HashMap<>();

    static {
        map.put("person", Person::new);
        map.put("manager", Manager::new);
        map.put("p2", Person::new);
        map.put("empolyee", Employee::new);
    }

    static Map<String, BiFunction<String, Integer, Person>> pMap = new HashMap<>();

    static {
        pMap.put("person", Person::new);
        pMap.put("manager", Manager::new);
        pMap.put("p2", Person::new);
        pMap.put("empolyee", Employee::new);
    }

    /**
     *
     * @param type
     * @param age
     * @return
     */
    public static Person giveMePerson(String type, Integer age) {
        return map.get(type).apply(age);
    }

    /**
     *
     * @param type
     * @param age
     * @return
     */
    public static Person giveMePersonWithParams(String type, Integer age) {
        return pMap.get(type).apply(type, age);
    }

    // using Supplier interface to produce an element using lambda
    // expression
    public static <T extends Person> T produceElementOfPerson(Supplier<T> s) {
        return s.get();
    }

    // get an element and transform it to another type
    public static <T, R> List<R> map(List<T> list, Function<T, R> f) {
        List<R> rs = new ArrayList<>(0);
        list.stream().forEach((i) -> {
            rs.add(f.apply(i));
        });
        return rs;
    }

    // get a collection and perform an action on each element
    public static <T> void forEach(List<T> list, Consumer<T> consumer) {
        list.stream().forEach((i) -> {
            consumer.accept(i);
        });
    }

    public static String processFile(BufferedReaderProcessor processor) {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(
                "src/java8inaction/ch3/data.txt")))) {
            return processor.process(br);
        } catch (IOException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static <T> List<T> abstractFilter(List<T> list, Predicate<T> p) {
        List<T> result = new ArrayList<>(0);
        list.stream().filter((e) -> (p.test(e))).forEach((e) -> {
            result.add(e);
        });
        return result;
    }

    @FunctionalInterface
    public interface BufferedReaderProcessor {

        String process(BufferedReader bufferedReader) throws IOException;
    }

    // exceptions with lambdas
    @FunctionalInterface
    public interface BufferedReaderProcessorIO {

        String process(BufferedReader reader) throws IOException;
    }

    private static class Person {

        private String name;
        private Integer age;

        public Person(Integer age) {
            this.age = age;
            this.name = "Name person";
        }

        /**
         * @param name
         * @param age
         */
        public Person(String name, Integer age) {
            super();
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person [name=" + name + ", age=" + age + "]";
        }

    }

    private static class Manager extends Person {

        public Manager(Integer age) {
            super(age);
            super.name = "Manger name";
        }

        public Manager(String name, Integer age) {
            super(name, age);
        }

        public Manager(String position) {
            super("", 12);
            this.position = position;
        }

        /**
         * @param position
         */
        public Manager(String name, Integer age, String position) {
            super(name, age);
            this.position = position;
        }

        private String position;

        public String getPosition() {
            return position;
        }

        @Override
        public String toString() {
            return "Manager [position=" + position + "]";
        }

    }

    private static class Employee extends Person {

        private String stage;

        public Employee(Integer age) {
            super(age);
            super.name = "Employee name";
        }

        public Employee(String name, Integer age) {
            super(name, age);
        }

        public Employee() {
            super("Default employee", 42);
            this.stage = "Default stage";
        }

        @Override
        public String toString() {
            return "Employee [stage=" + stage + "]";
        }

    }

    @FunctionalInterface
    interface TriFunction<T, U, V, R> {

        R apply(T t, U u, V v);

        default TriFunction<T, U, V, R> andThen(TriFunction<T, U, V, R> after) {
            Objects.requireNonNull(after);
            return (t, u, v) -> after.apply(t, u, v);
        }

    }

}
