package java8inaction.ch2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FilteringApples {

    public static void main(String[] args) {
        List<Apple> inventory = Arrays.asList(new Apple(20, "red"), new Apple(
                200, "green"), new Apple(155, "blue"), new Apple(120, "green"));

        // Apple [weight=20, color=red]
        List<Apple> redApples = filterApplesByColor(inventory, "red");
        System.out.println(redApples);

        // Apple [weight=200, color=green], Apple [weight=120, color=green]
        List<Apple> greenApples = filterApplesByColor(inventory, "green");
        System.out.println(greenApples);

        // Apple [weight=200, color=green], Apple [weight=155, color=blue]
        List<Apple> greaterThan150Apples = filterApplesByWeight(inventory, 150);
        System.out.println(greaterThan150Apples);

        // Apple [weight=200, color=green], Apple [weight=120, color=green]
        List<Apple> greenApplesFilter = filter(inventory,
                new AppleColorPredicate());
        System.out.println(greenApplesFilter);

        // []
        List<Apple> greaterThan150ApplesAndRedFilter = filter(inventory,
                new AppleRedAndHeavyPredicate());
        System.out.println(greaterThan150ApplesAndRedFilter);

        // using anonymous class
        List<Apple> greatherThan20AndBlue = filter(inventory,
                new ApplePredicate() {
                    @Override
                    public boolean test(Apple a) {
                        return a.getWeight() > 20
                        && a.getColor().equals("blue");
                    }
                });
        // Apple [weight=155, color=blue]
        System.out.println(greatherThan20AndBlue);

        // using lambda
        List<Apple> blueApples = filter(inventory,
                (Apple a) -> "blue".equals(a.getColor()));

        // Apple [weight=155, color=blue]
        System.out.println(blueApples);

        // using lambda with abstract filter for Apple object
        List<Apple> redOrBlueApples = abstractFilter(
                inventory,
                (Apple apple) -> "blue".equals(apple.getColor())
                || "red".equals(apple.getColor()));
        // Apple [weight=20, color=red] Apple [weight=155, color=blue]
        System.out.println(redOrBlueApples);

        // using lambda with abstract filter for String object
        List<String> listStrings = Arrays.asList("Oleg", "Marusea",
                "Vara merge pe drum", "Valea este mare si muntele inalt",
                "Java");
        List<String> moreThan10Chrs = abstractFilter(listStrings,
                (String s) -> s.length() > 10);

        // [Vara merge pe drum Valea este mare si muntele inalt]
        System.out.println(moreThan10Chrs);

        // sort apples using anonymous class
        inventory.sort(new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.getWeight().compareTo(o2.getWeight());
            }
        });

        System.out.println(inventory);
        // sort apples using lambda expression
        inventory.sort((Apple a1, Apple a2) -> a2.getColor().compareTo(
                a1.getColor()));
        System.out.println(inventory);
        List<String> listOfNotEmptyStrings = abstractFilter(listStrings, (
                String s) -> !s.isEmpty());
        System.out
                .println("List of not empty strings:" + listOfNotEmptyStrings);

        // execute a block of code with Runnable
        Thread t = new Thread(() -> System.out.println("Hello World!"));
        t.start();

        // QUIZ
        System.out.println("START QUIZ");
        // red green blue green
        prettyPrintApple(inventory, new PrintColorApple());
        // red green blue green
        prettyPrintApple(inventory, (Apple a) -> a.getColor());

        // 20 200 155 120
        prettyPrintApple(inventory, new PrintWeightApple());

        // Apple [weight=20, color=red], Apple [weight=200, color=green], Apple
        // [weight=155, color=blue], Apple [weight=120, color=green]
        prettyPrintApple(inventory, new PrintFieldsApple());
        System.out.println("\nEND QUIZ");
    }

    // interfata functionala deoarece are doar o singura metoda
    public static interface Predicate<T> {

        boolean test(T t);
    }

    public static <T> List<T> abstractFilter(List<T> list, Predicate<T> p) {
        List<T> result = new ArrayList<T>(0);
        for (T e : list) {
            if (p.test(e)) {
                result.add(e);
            }
        }
        return result;
    }

    // filtering apples with variable color
    public static List<Apple> filterApplesByColor(List<Apple> inventory,
            String color) {
        List<Apple> result = new ArrayList<Apple>(0);
        for (Apple apple : inventory) {
            if (apple.getColor().equals(color)) {
                result.add(apple);
            }
        }

        return result;
    }

    // filtering green apples from a list
    public static List<Apple> filterGreenApples(List<Apple> inventory) {
        List<Apple> result = new ArrayList<Apple>(0);
        for (Apple apple : inventory) {
            if ("green".equals(apple.getColor())) {
                result.add(apple);
            }
        }

        return result;
    }

    public static List<Apple> filterApplesByWeight(List<Apple> inventory,
            int weight) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if (apple.getWeight() > weight) {
                result.add(apple);
            }
        }
        return result;
    }

    public static List<Apple> filter(List<Apple> inventory, ApplePredicate p) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple : inventory) {
            if (p.test(apple)) {
                result.add(apple);
            }
        }
        return result;
    }

    public static class Apple {

        private int weight = 0;
        private String color;

        /**
         * @param weight
         * @param color
         */
        public Apple(int weight, String color) {
            super();
            this.weight = weight;
            this.color = color;
        }

        public Apple(int weight, AppleColor color) {
            super();
            this.weight = weight;
            this.color = color.name();
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "Apple [weight=" + weight + ", color=" + color + "]";
        }

    }

    public enum AppleColor {

        green, red, blue
    }

    @FunctionalInterface
    interface ApplePredicate {

        public boolean test(Apple a);
    }

    static class AppleWeightPredicate implements ApplePredicate {

        @Override
        public boolean test(Apple apple) {
            return apple.getWeight() > 150;
        }
    }

    static class AppleColorPredicate implements ApplePredicate {

        @Override
        public boolean test(Apple apple) {
            return "green".equals(apple.getColor());
        }
    }

    static class AppleRedAndHeavyPredicate implements ApplePredicate {

        @Override
        public boolean test(Apple apple) {
            return "red".equals(apple.getColor()) && apple.getWeight() > 150;
        }
    }

    // ----- start QUIZ ----
    // print apples
    public static void prettyPrintApple(List<Apple> inventory,
            PrintApplePredicate p) {
        for (Apple apple : inventory) {
            String output = p.printApple(apple);
            System.out.print(output + " ");
        }
        System.out.println();
    }

    interface PrintApplePredicate {

        String printApple(Apple apple);
    }

    public static class PrintColorApple implements PrintApplePredicate {

        @Override
        public String printApple(Apple apple) {
            return apple.getColor();
        }
    }

    public static class PrintWeightApple implements PrintApplePredicate {

        @Override
        public String printApple(Apple apple) {
            return String.valueOf(apple.getWeight());
        }
    }

    public static class PrintFieldsApple implements PrintApplePredicate {

        @Override
        public String printApple(Apple apple) {
            return apple.toString();
        }
    }

	// -----end QUIZ -------
}
