package java8inaction;

import java.io.File;
import java.io.FileFilter;

public class Main {

    /*
     * Programming concepts added in Java 8
     *
     * (1) Streams
     *
     * A stream is a sequence of data items that are conceptually produced one
     * at a time—a program might read items from an input stream one by one and
     * similarly write items to an output stream.
     *
     * (2) Behavior parameterization Ability to pass a piece of code to an API
     *
     * (3) Parallelism and shared mutable data “parallelism almost for free”
     *
     * (4) Methods and lambdas as first-class citizens
     *
     * (5) Default methods
     */
    public static void main(String[] args) {
        /*
         * filtering hidden files
         */
        new File(".").listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isHidden();
            }
        });
        // <==>
        new File(".").listFiles(File::isHidden);

        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);
    }
}
