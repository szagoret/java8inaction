package java8inaction.ch4;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author Sergiu
 * @category Streams are an update to the Java API that lets you manipulate collections of data in a
 * declarative way (you express a query rather than code an ad hoc implementation for it).
 */
public class UsingStreams {

    public static void main(String[] args) {
        List<Dish> menu = Arrays.asList(new Dish("pork", false, 800,
                Dish.Type.MEAT), new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT), new Dish(
                        "french fries", true, 530, Dish.Type.OTHER), new Dish(
                        "rice", true, 350, Dish.Type.OTHER), new Dish(
                        "season fruit", true, 120, Dish.Type.OTHER), new Dish(
                        "pizza", true, 550, Dish.Type.OTHER), new Dish(
                        "prawns", false, 300, Dish.Type.FISH), new Dish(
                        "salmon", false, 450, Dish.Type.FISH));

        // get a stream from menu (the list of dishes)
        // Create a pipeline of operations
        // (1) Filter elements
        // (2) Get the names of the dishes
        // (3) Select only first three
        // (4) Store the result in another List
        List<String> threeHighCaloricDishNames = menu.stream().filter(d -> {
            System.out.println("filtering:" + d.getName());
            return d.getCalories() < 300;
        }).map(d -> {
            System.out.println("mapping:" + d.getName());
            return d.getName();
        }).limit(3).collect(toList());
        System.out.println("====================================");
        System.out.println("Three High Caloric Dish Names:"
                + threeHighCaloricDishNames);

        long count = menu.stream().distinct().limit(3).count();
        System.out.println("First three elements in collection:" + count);
    }
}
