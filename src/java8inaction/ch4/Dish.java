package java8inaction.ch4;

import java8inaction.ch6.CollectingStreams;

public class Dish {

    private String name;
    private Boolean vegetarian;
    private int calories;
    private Type type;

    /**
     * @param name
     * @param vegetarian
     * @param calories
     * @param type
     */
    public Dish(String name, Boolean vegetarian, int calories, Type type) {
        super();
        this.name = name;
        this.vegetarian = vegetarian;
        this.calories = calories;
        this.type = type;
    }

    public CollectingStreams.CaloricLevel getCaloricLevel() {
        if (this.getCalories() <= 400) {
            return CollectingStreams.CaloricLevel.DIET;
        } else if (this.getCalories() <= 700) {
            return CollectingStreams.CaloricLevel.NORMAL;
        } else {
            return CollectingStreams.CaloricLevel.FAT;
        }
    }

    public String getName() {
        return name;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public Boolean isVegetarian() {
        return this.vegetarian;
    }

    public int getCalories() {
        return calories;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return name;
    }

    public enum Type {

        MEAT, FISH, OTHER
    }
}
