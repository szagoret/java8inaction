package java8inaction.ch9;

/**
 *
 * @author szagoret
 */
public class DefaultMethods {

    public static void main(String[] args) {
        Monster monster = new Monster();
        monster.rotateBy(180);
        monster.moveVertically(10);
    }
}
