package java8inaction.ch9.diamond;

/**
 *
 * @author szagoret
 */
public class Main implements B, A {

    public static void main(String[] args) {
        /**
         * There’s actually only one method declaration to choose from. Only A declares a default
         * method. Because the interface is a super interface of D, the code will print “Hello from
         * * A.”
         */
        new Main().hello();
    }
}

interface A {

    default void hello() {
        System.out.println("Hello form A");
    }
}

interface B extends A {
}

interface C extends A {
}
