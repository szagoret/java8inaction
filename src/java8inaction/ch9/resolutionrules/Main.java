package java8inaction.ch9.resolutionrules;

/**
 *
 * @author szagoret
 */
public class Main implements A, B {

    /**
     * explicitly decide which method declaration you want to use otherwise you have a compile error
     */
    @Override
    public void hello() {
        B.super.hello();
    }
}

interface A {

    default void hello() {
        System.out.println("Hello from A");
    }
}

interface B {

    default void hello() {
        System.out.println("Hello from B");
    }
}
