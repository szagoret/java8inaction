package java8inaction.ch5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.joining;

/**
 *
 * @author szagoret
 */
public class ManageTransactionsQuiz {

    public static void main(String[] args) {

        Trader raoul = new Trader("Roul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(mario, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 710),
                new Transaction(alan, 2012, 950)
        );

        // (1) Find all transactions in the 2011 and sort them by value (small to high).
        List<Transaction> result1 = transactions.stream().filter((t) -> t.getYear() == 2011)
                .sorted(Comparator.comparing(Transaction::getValue)).collect(Collectors.toList());
        System.out.println("Result 1: " + result1);

        // (2) What are all the unique cities where the traders work?
        List<String> result2 = transactions.stream().map((t) -> t.getTrader().getCity())
                .distinct().collect(Collectors.toList());
        System.out.println("Result 2: " + result2);

        // (3) Find all traders from Cambridge and sort them by name
        List<Trader> result3 = transactions.stream().filter((t) -> t.getTrader().getCity()
                .equals("Cambridge")).map((t) -> t.getTrader()).distinct()
                .sorted(Comparator.comparing((Trader::getName)))
                .collect(Collectors.toList());
        System.out.println("Result 3: " + result3);

        // (4) Return a string of all traders'names sorted alphabeticaly.
        String result4 = transactions.stream()
                .map((t) -> t.getTrader().getName())
                .distinct()
                .sorted().reduce("", (a, b) -> a.concat(" " + b));
        System.out.println("Result 4: " + result4);

        String result41 = transactions.stream()
                .map((t) -> t.getTrader().getName())
                .distinct()
                .sorted().collect(joining());
        System.out.println("Result 41: " + result41);

        // (5) Are any traders based in Milan?
        Boolean result5 = transactions.stream().allMatch((t) -> t.getTrader().getCity().equals("Milan"));
        System.out.println("Result 5: " + result5);

        // (6) Print all transactions'values from the traders living in Cambridge.
        List<Integer> result6 = transactions.stream().filter((t) -> t.getTrader().getCity().equals("Cambridge"))
                .map((t) -> t.getValue()).collect(Collectors.toList());
        System.out.println("Result 6: " + result6);

        // (7) What's the highest value of all the transactions?
        Optional<Integer> result7 = transactions.stream().map(Transaction::getValue)
                .reduce(Integer::max);
        System.out.println("Result 7: " + result7.get());

        Transaction result8 = transactions.stream().reduce(transactions.get(0), (a, b) -> {
            return a.getValue() < b.getValue() ? a : b;
        });
        System.out.println("Result 8: " + result8);

        Optional<Transaction> result81 = transactions.stream().min(Comparator.comparing(Transaction::getValue));
        System.out.println("Result 81: " + result81.get());
    }

    private static class Trader {

        private final String name;
        private final String city;

        public Trader(String n, String c) {
            this.name = n;
            this.city = c;
        }

        public String getName() {
            return name;
        }

        public String getCity() {
            return city;
        }

        @Override
        public String toString() {
            return "Trader:" + this.name + " in " + this.city;
        }

    }

    private static class Transaction {

        private final Trader trader;
        private final int year;
        private final int value;

        public Transaction(Trader trader, int year, int value) {
            this.trader = trader;
            this.year = year;
            this.value = value;
        }

        public Trader getTrader() {
            return trader;
        }

        public int getValue() {
            return value;
        }

        public int getYear() {
            return year;
        }

        @Override
        public String toString() {
            return "{" + this.trader + ", " + " year: " + this.year + " value: " + this.value + "}";
        }

    }

}
