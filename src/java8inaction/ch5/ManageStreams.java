package java8inaction.ch5;

import java.io.IOException;
import java.util.Arrays;
import static java.util.Comparator.comparing;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.IntSupplier;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java8inaction.ch4.Dish;

/**
 *
 * @author Sergiu
 *
 */
public class ManageStreams {

    public static void main(String[] args) throws IOException {

        List<Dish> menu = Arrays.asList(new Dish("pork", false, 800,
                Dish.Type.MEAT), new Dish("beef", false, 320, Dish.Type.MEAT),
                new Dish("chicken", false, 200, Dish.Type.MEAT), new Dish(
                        "french fries", true, 530, Dish.Type.OTHER), new Dish(
                        "rice", true, 350, Dish.Type.OTHER), new Dish(
                        "season fruit", true, 120, Dish.Type.OTHER), new Dish(
                        "pizza", true, 550, Dish.Type.OTHER), new Dish(
                        "prawns", false, 300, Dish.Type.FISH), new Dish(
                        "salmon", false, 450, Dish.Type.FISH));

        // Filtering with a predicate
        // get vegetarian menu
        List<Dish> vegetarianMenu = menu.stream().filter(Dish::isVegetarian)
                .collect(toList());
        System.out.println("Vegetarian menu:" + vegetarianMenu);

        // Filtering unique elements in a stream
        List<Integer> integerList = Arrays.asList(1, 2, 3, 5, 1, 2, 6, 7, 8, 1);

        integerList.stream().filter(i -> i % 2 == 0).distinct()
                .forEach(System.out::println);

        // Truncating a stream
        // selecting the first three dishes that have more than 300 calories
        List<Dish> truncatedList = menu.stream()
                .filter(d -> d.getCalories() > 300).limit(3)
                .sorted(comparing(Dish::getCalories)).collect(toList());
        System.out.println("First three elements with more than 300 cal : "
                + truncatedList);

        // Skipping elements
        // skip first two elements with calories > 400
        List<Dish> skipedElements = menu.stream().filter(d -> d.getCalories() > 300).skip(2).collect(toList());
        System.out.println("Not skiped elements are:" + skipedElements);

        // Mapping
        List<String> words = Arrays.asList("Java8", "Lambdas", "In", "Action");
        List<Integer> wordLengths = words.stream().map(String::length).collect(toList());
        System.out.println("Words are: " + words + "\n" + "Words length are: " + wordLengths);

        List<Integer> dishNameLengths = menu.stream().map(Dish::getName).map(String::length).collect(toList());
        System.out.println("Dish length are: " + dishNameLengths);

        // Flattening streams -> join all streams into a single stream
        // return a list of all unique characters for a list of words
        List<String> distinctChars = words.stream().map(word -> word.split("")).flatMap(Arrays::stream).distinct().collect(toList());
        System.out.println("Words by characters : " + distinctChars);

        /**
         * Quiz Mapping
         */
        // (1) -> square of each element in the list
        List<Integer> quizNumbers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> squareNumbers = quizNumbers.stream().map(number -> number * number).collect(toList());
        System.out.println("Square of each number: " + squareNumbers);

        // (2) -> Get all pairs of numbers in two lists
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(3, 4);

        List<int[]> pairsNumbers = list1.stream().flatMap(i -> list2.stream().map(j -> new int[]{i, j})).collect(toList());

        /**
         * Find and matching
         */
        if (menu.stream().anyMatch(Dish::isVegetarian)) {
            System.out.println("The menu is (somewhat) vegetarian friendly !!!");
        }

        boolean isHealthy = menu.stream().allMatch(d -> d.getCalories() < 1000);
        System.out.println("Is Healthy: " + isHealthy);

        boolean isNotHealthy = menu.stream().noneMatch(d -> d.getCalories() >= 1000);
        System.out.println("Is Healthy: " + isNotHealthy);

        // Finding an element
        Optional<Dish> optDish = menu.stream().filter(Dish::isVegetarian).findAny();
        menu.stream().filter(Dish::isVegetarian).findAny().ifPresent(d -> System.out.println(d.getName()));

        // Finding the firts element
        List<Integer> someNumbers = Arrays.asList(1, 2, 3, 4, 5);
        Optional<Integer> firstSquareDivisibleByThree = someNumbers.stream().map(x -> x * x).filter(x -> x % 3 == 0).findFirst();
        firstSquareDivisibleByThree.ifPresent(d -> System.out.println(d));

        /**
         * Reducing
         */
        // Summing the elements
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

//        int sum = numbers.stream().reduce(0, (a, b) -> a + b);
        int sum = numbers.stream().reduce(0, Integer::sum);
        System.out.println("Sum of numbers:" + sum);

        int product = numbers.stream().reduce(1, (a, b) -> a * b);
        System.out.println("Product of numbers:" + product);

        // Maximum an Minimum
        int maximumInt = numbers.stream().reduce(numbers.get(0), Integer::max);
        System.out.println("Maximum number is :" + maximumInt);

        int minimumInt = numbers.stream().reduce(numbers.get(0), Integer::min);
        System.out.println("Minimum number is :" + minimumInt);

        /**
         * Primitive Streams
         */
        int calories = menu.stream().mapToInt(Dish::getCalories).sum();
        System.out.println("Sum calories: " + calories);

        // Converting a Stream to a numeric stream
        IntStream intStream = menu.stream().mapToInt(Dish::getCalories);

        // Converting the numeric stream to a Stream
        Stream<Integer> stream = intStream.boxed();

        // Default values: OptionalInt
        OptionalInt maxCalories = menu.stream().mapToInt(Dish::getCalories).max();

        //Provide an explicit default maximum if there's o value
        int max = maxCalories.orElse(1);

        /**
         * Numeric ranges
         */
        IntStream evenNumbers = IntStream.rangeClosed(1, 100).filter(n -> n % 2 == 0);
        System.out.println("Even numbers count between 1 and 100 : " + evenNumbers.count());

        // Pythagorean triple
        Stream<int[]> pythagoreanTriples = IntStream.rangeClosed(1, 100)
                .boxed()
                .flatMap(a -> IntStream.rangeClosed(a, 100)
                        .filter(b -> Math.sqrt(a * a + b * b) % 1 == 0)
                        .mapToObj(b -> new int[]{a, b, (int) Math.sqrt(a * a + b * b)})
                );
        pythagoreanTriples.limit(5).forEach(t -> {
            System.out.println(t[0] + ", " + t[1] + ", " + t[2]);
        });
        System.out.println("|===================|");
        Stream<int[]> pythagoreanTriplesO = IntStream.rangeClosed(1, 100)
                .boxed()
                .flatMap(a -> IntStream.rangeClosed(a, 100)
                        .mapToObj(b -> new int[]{a, b, (int) Math.sqrt(a * a + b * b)})
                        .filter(t -> t[2] % 1 == 0)
                );
        pythagoreanTriplesO.limit(5).forEach(t -> {
            System.out.println(t[0] + ", " + t[1] + ", " + t[2]);
        });
        /**
         * Streams from functions: creating infinite streams
         */
        System.out.println("Limited stream:");
        // 0 (zero) - initial value
        Stream.iterate(0, n -> n + 2).limit(10).forEach(System.out::println);

        // Generate the first 20 elements of the series of Fibonacci tuples
        // using the iterate method
        System.out.println("Fibonacci numbers: ");
        Stream.iterate(new int[]{0, 1}, n -> new int[]{n[1], n[0] + n[1]}).limit(10)
                .forEach(t -> System.out.println("(" + t[0] + ", " + t[1] + ")"));

        System.out.println("Fibonacci series:");
        Stream.iterate(new int[]{0, 1}, n -> new int[]{n[1], n[0] + n[1]}).limit(10).map(t -> t[0])
                .forEach(System.out::println);

        // Generate
        System.out.println("Generate numbers:");
        Stream.generate(Math::random).limit(5).forEach(System.out::println);

        System.out.println("Fibonacci numbers using generate:");
        // Fibonacci numbers using generate
        IntSupplier fib = new IntSupplier() {

            private int previous = 0;
            private int current = 1;

            @Override
            public int getAsInt() {
                int oldPrevious = this.previous;
                int nextValue = this.previous + this.current;
                this.previous = this.current;
                this.current = nextValue;
                return oldPrevious;
            }
        };

        IntStream.generate(fib).limit(10).forEach(System.out::println);
    }
}
