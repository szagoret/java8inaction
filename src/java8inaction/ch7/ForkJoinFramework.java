package java8inaction.ch7;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.LongStream;

/**
 *
 * @author szagoret
 *
 * Work stealing -> cind impartim in multe subtask-uri mici, fiecare thread are un double-queue de
 * task-uri care le executa, daca un thread si-a terminat mai repede task-urile el nu trece in
 * asteptare, dar alege aleatoriu un alt queue de task-uri de la alt thread si il ajuta sa le
 * execute
 *
 */
public class ForkJoinFramework {

    public static void main(String[] args) {

        System.out.println("ForkJoin sum done in: "
                + MeasuringPerformance.measureSumPerf(ForkJoinFramework::forkJoinSum, 10000000) + " msec");
    }

    public static long forkJoinSum(long n) {
        long[] numbers = LongStream.rangeClosed(1, n).toArray();
        ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers);
        return new ForkJoinPool().invoke(task);
    }
}

/**
 *
 * Extend RecursiteTask to create a task usable with the fork/join framework
 */
class ForkJoinSumCalculator extends java.util.concurrent.RecursiveTask<Long> {

    // The array of numbers to be summed
    private final long[] numbers;

    /**
     * The initial and final positions of the portion of the array processed
     */
    private final int start;
    private final int end;

    // The size of the array under wich this task is no longer split into subtask
    public static final long THRESHOLD = 10000;

    //private constructor used to recursively create subtasks of the main task
    private ForkJoinSumCalculator(long[] numbers, int start, int end) {
        this.numbers = numbers;
        this.start = start;
        this.end = end;
    }

    //public constructor used to create the main task
    public ForkJoinSumCalculator(long[] numbers) {
        this(numbers, 0, numbers.length);
    }

    // The size of the portion of the array summed by this task
    @Override
    protected Long compute() {
        int length = end - start;
        if (length <= THRESHOLD) {
            return computeSequentialy();
        }

        //Create a subtask to sum the first half of the array
        ForkJoinSumCalculator leftTask = new ForkJoinSumCalculator(numbers, start, start + length / 2);

        /**
         * Asynchronously execute the newly created subtask using another thread of the ForkJoinPool
         * Calling the fork method on a subtask is the way to schedule it on the ForkJoinPool. It
         * might seem natural to invoke it on both the left and right subtasks, but this is less
         * efficient than just directly calling compute on one of them. Doing this allows you to
         * reuse the same thread for one of the two subtasks and avoid the overhead caused by the
         * unnecessary allocation of a further task on the pool.
         */
        leftTask.fork();

        //Create a subtask to sum the second half of the array
        ForkJoinSumCalculator rightTask = new ForkJoinSumCalculator(numbers, start + length / 2, end);

        // Execute this second subtask synchronusly, potentialy allowing further recursive splits
        Long rightResult = rightTask.compute();

        // Read the result of the first subtask or wait for it if it isn't ready
        Long leftResult = leftTask.join();

        // The result of this task is the combination of the results of the two subtasks
        return leftResult + rightResult;
    }

    // Simple algorithm calculatoing the result of a subtask when it's no longer divisible
    private Long computeSequentialy() {
        long sum = 0;
        for (int i = start; i < end; i++) {
            sum += numbers[i];
        }
        return sum;
    }

}
