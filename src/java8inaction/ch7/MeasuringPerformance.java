package java8inaction.ch7;

import java.util.function.Function;
import java.util.stream.LongStream;

/**
 *
 * @author szagoret
 */
public class MeasuringPerformance {

    public static void main(String[] args) {
        //  System.out.println("Sequential ranged sum done: " + measureSumPerf(ParalelStreams::rangedSum, Integer.MAX_VALUE) + " msec");
        //  System.out.println("Parallel  ranged sum done: " + measureSumPerf(ParalelStreams::paralelRangedSum, Integer.MAX_VALUE) + " msec");
        System.out.println("Sequential sum done in: " + measureSumPerf(ParalelStreams::sequentialSideEffectSum, 10) + " msec");
        System.out.println("SideEffect parallel sum done in: " + measureSumPerf(ParalelStreams::parallelSideEffectSum, 10) + " msec");
        //  System.out.println("Sequential find any: " + measureSumPerf(ParalelStreams::sequentialFindAny, Integer.MAX_VALUE) + " msec");
        //  System.out.println("Parallel find any: " + measureSumPerf(ParalelStreams::parallelFindAny, Integer.MAX_VALUE) + " msec");
    }

    public static long measureSumPerf(Function<Long, Long> adder, long n) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            long sum = adder.apply(n);
            long duration = (System.nanoTime() - start) / 1000000; //milisecond
            System.out.println("Result: " + sum);
            if (duration < fastest) {
                fastest = duration;
            }
        }
        return fastest;
    }

    static class ParalelStreams {

        public static long parallelFindAny(long n) {
            return LongStream.rangeClosed(0, n).parallel().findAny().getAsLong();
        }

        public static long sequentialFindAny(long n) {
            return LongStream.rangeClosed(0, n).findAny().getAsLong();
        }

        public static long rangedSum(long n) {
            return LongStream.rangeClosed(1, n).reduce(0L, Long::sum);
        }

        public static long paralelRangedSum(long n) {
            return LongStream.rangeClosed(1, n).parallel().reduce(0L, Long::sum);
        }

        public static long parallelSideEffectSum(long n) {
            Accumulator accumulator = new Accumulator();
            LongStream.rangeClosed(1, n).parallel().forEach(accumulator::add);
            return accumulator.total;
        }

        public static long sequentialSideEffectSum(long n) {
            Accumulator accumulator = new Accumulator();
            LongStream.rangeClosed(1, n).forEach(accumulator::add);
            return accumulator.total;
        }

    }

    static class Accumulator {

        public long total = 0;

        public void add(long value) {
            total += value;
        }
    }
}
