package java8inaction.ch7;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

/**
 *
 * @author szagoret
 */
public class ConcurrentMergeSort {

    public static void main(String[] args) {
        Integer[] array = new Random().ints(1000000, 1000000, 10000000).boxed().collect(Collectors.toList()).toArray(new Integer[0]);
        Integer[] arrayForConcurrent = Arrays.copyOf(array, array.length);
        Integer[] arrayForStream = Arrays.copyOf(array, array.length);

        long start = System.nanoTime();

        mergeSort(array, (Integer n1, Integer n2) -> n1.compareTo(n2));

        long duration = (System.nanoTime() - start) / 1000000; //milisecond
        System.out.println("Sorted array sequentialy: " + duration + " msec");

        start = System.nanoTime();

        ForkJoinMergeSort.concurrentMergeSort(arrayForConcurrent);
        duration = (System.nanoTime() - start) / 1000000; //milisecond
        System.out.println("Concurrent array sequentialy: " + duration + " msec");

        start = System.nanoTime();
        Arrays.stream(arrayForStream).sorted();
        duration = (System.nanoTime() - start) / 1000000; //milisecond
        System.out.println("Java 8 stream sort: " + duration + " msec");
        System.out.println();
    }

    /**
     * Merge-sort contents of array S.
     */
    public static <K> void mergeSort(K[] S, Comparator<K> comp) {
        int n = S.length;
        if (n < 2) {
            return;
        }

        int mid = n / 2;

        K[] S1 = Arrays.copyOfRange(S, 0, mid); // copy of first half
        K[] S2 = Arrays.copyOfRange(S, mid, n); // copy of second half

        //conquer (with recursion)
        mergeSort(S1, comp); // sort copy of first half
        mergeSort(S2, comp); // osrt copy of second half

        // merge result
        merge(S1, S2, S, comp);

    }

    /**
     * Merge contents of arrays S1 and S2 into properly sized array S.
     */
    public static <K> void merge(K[] S1, K[] S2, K[] S, Comparator<K> comp) {
        int i = 0, j = 0;
        while (i + j < S.length) {
            if (j == S2.length || i < S1.length && comp.compare(S1[i], S2[j]) < 0) {
                S[i + j] = S1[i++];
            } else {
                S[i + j] = S2[j++];
            }
        }
    }

}

class ForkJoinMergeSort<T extends Comparable<? super T>> extends RecursiveAction {

    private final T[] array;
    private final T[] helper;
    private final int lo;
    private final int hi;

    public ForkJoinMergeSort(T[] array, T[] helper, int lo, int hi) {
        this.array = array;
        this.helper = helper;
        this.lo = lo;
        this.hi = hi;
    }

    @Override
    protected void compute() {

        if (lo >= hi) {
            return;
        }

        int mid = lo + (hi - lo) / 2;
        ForkJoinMergeSort<T> left = new ForkJoinMergeSort<>(array, helper, lo, mid);
        ForkJoinMergeSort<T> right = new ForkJoinMergeSort<>(array, helper, mid + 1, hi);
        invokeAll(left, right);
        merge(this.array, this.helper, this.lo, mid, this.hi);
    }

    private void merge(T[] a, T[] helper, int lo, int mid, int hi) {
        for (int i = lo; i <= hi; i++) {
            helper[i] = a[i];
        }
        int i = lo, j = mid + 1;
        for (int k = lo; k <= hi; k++) {
            if (i > mid) {
                a[k] = helper[j++];
            } else if (j > hi) {
                a[k] = helper[i++];
            } else if (isLess(helper[i], helper[j])) {
                a[k] = helper[i++];
            } else {
                a[k] = helper[j++];
            }
        }
    }

    private boolean isLess(T a, T b) {
        return a.compareTo(b) < 0;
    }

    public static <T extends Comparable<? super T>> void concurrentMergeSort(T[] a) {
        T[] helper = (T[]) Array.newInstance(a[0].getClass(), a.length);
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);
        forkJoinPool.invoke(new ForkJoinMergeSort<T>(a, helper, 0, a.length - 1));
    }
}
