package java8inaction.ch7;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author szagoret
 */
public class CustomSpliterator {

    public static void main(String[] args) {
        final String SENTENCE = " Nel mezzo del cammin di nostra vita "
                + "mi ritrovai in    una selva oscura"
                + " ché la dritta via era smarrita ";
        System.out.println("Found " + countWordsIteratively(SENTENCE) + " words");

        Stream<Character> stream = IntStream.range(0, SENTENCE.length()).mapToObj(SENTENCE::charAt);
//        System.out.println("Found " + countWords(stream) + " words");
        System.out.println("Parallel Found " + countWords(stream.parallel()) + " words");
    }

    /**
     * Traverse all the characters and count spaces
     */
    public static int countWordsIteratively(String s) {
        int counter = 0;
        boolean lastSpace = true;
        for (char c : s.toCharArray()) {
            if (Character.isWhitespace(c)) {
                lastSpace = true;
            } else {
                if (lastSpace) {
                    counter++;
                }
                lastSpace = false;
            }
        }
        return counter;
    }

    public static int countWords(Stream<Character> stream) {
        return stream.reduce(new WordCounter(0, true),
                WordCounter::accumulate, WordCounter::combine)
                .getCounter();
    }

    static class WordCounter {

        private final int counter;
        private final boolean lastSpace;

        public WordCounter(int counter, boolean lastSpace) {
            this.counter = counter;
            this.lastSpace = lastSpace;
        }

        // The accumulate method traverses the Characters one by one
        // as done by the iterative algorithm
        public WordCounter accumulate(Character c) {
            if (Character.isWhitespace(c)) {
                return lastSpace ? this : new WordCounter(counter, true);
            } else {
                // Increase the word counter when the last character is a space and
                // the currently traversed one isn't
                return lastSpace ? new WordCounter(counter + 1, false) : this;
            }
        }

        // Combine two WordCounters by summing their counters
        public WordCounter combine(WordCounter wordCounter) {
            // User only the sum of the counters so you don't care about lastSpace
            return new WordCounter(counter + wordCounter.counter, wordCounter.lastSpace);
        }

        public int getCounter() {
            return counter;
        }
    }

    static class WordCounterSpliterator implements Spliterator<Character> {

        private final String string;
        private int currentChar = 0;

        public WordCounterSpliterator(String string) {
            this.string = string;
        }

        @Override
        public boolean tryAdvance(Consumer<? super Character> action) {
            //Consume the current character
            action.accept(string.charAt(currentChar++));
            // Return true if there are futher characters to be consumed
            return currentChar < string.length();
        }

        @Override
        public Spliterator<Character> trySplit() {
            int currentSize = string.length() - currentChar;
            // Return null to signal that the String to be parsed
            // is small enough to be processed sequentialy
            if (currentSize < 10) {
                return null;
            }
            // Set the candidate's split position to be half of the String to be parsed
            for (int splitPos = currentSize / 2 + currentSize; splitPos < string.length(); splitPos++) {

                //Advance the split position until the next space
                if (Character.isWhitespace(string.charAt(splitPos))) {
                    // Create a new WordCounterSpliterator parsing
                    // the String from start to split position
                    Spliterator<Character> spliterator
                            = new WordCounterSpliterator(string.substring(currentChar, splitPos));

                    // Set the start position of this WordCounterSpliterator to the split position
                    currentChar = splitPos;
                    return spliterator;
                }
            }
            return null;
        }

        @Override
        public long estimateSize() {
            return string.length() - currentChar;
        }

        @Override
        public int characteristics() {
            return ORDERED + SIZED + SUBSIZED + NONNULL + IMMUTABLE;
        }

    }
}
