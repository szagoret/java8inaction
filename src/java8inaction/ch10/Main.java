package java8inaction.ch10;

import java.util.Optional;
import java.util.Properties;

/**
 *
 * @author szagoret
 */
public class Main {

    public static void main(String[] args) {
        Insurance insurance = new Insurance();
        Person person = new Person();
        Car car = new Car();
        insurance.setName("Company 1");
        car.setInsurance(insurance);
        person.setCar(Optional.ofNullable(car));
        /**
         * Without optional
         */
        String name = null;
        if (insurance != null) {
            name = insurance.getName();
        }

        /**
         * With optional
         */
        Optional<Insurance> optInsurance = Optional.ofNullable(insurance);
        Optional<String> optName = optInsurance.map(Insurance::getName);

        Optional<Person> optPerson = Optional.of(person);
//        Optional<String> optChaineName = optPerson.flatMap(Person::getCar).flatMap(Car::getInsurance)
//                .map(Insurance::getName);
        System.out.println("Optional Person: " + new Main().getCarInsuranceName(optPerson));

        /**
         * Quiz
         */
        Properties properties = new Properties();
        properties.setProperty("a", "5");
        properties.setProperty("b", "true");
        properties.setProperty("c", "-3");
        System.out.println("Duration of a:" + readDuration(properties, "a"));
        System.out.println("Duration of b:" + readDuration(properties, "b"));
        System.out.println("Duration of d:" + readDuration(properties, "d"));
    }

    public String getCarInsuranceName(Optional<Person> person) {
        return person.flatMap(Person::getCar).flatMap(Car::getInsurance)
                .map(Insurance::getName).orElse("Unknown");
    }

    public Optional<Insurance> nullSafeFindCheapestInsurance(Optional<Person> person, Optional<Car> car) {
        return person.flatMap(p -> car.map(c -> findCheapestInsurance(p, c)));
    }

    public Insurance findCheapestInsurance(Person person, Car car) {
        //queries services provided ny the different insurance companies
        //compare all those data
        return new Insurance();
    }

    public static int readDuration(Properties properties, String s) {
        return Optional.ofNullable(properties.getProperty(s))
                .flatMap(OptionalUtility::stringToInt).filter(i -> i > 0).orElse(0);
    }
}

class OptionalUtility {

    public static Optional<Integer> stringToInt(String s) {
        try {
            return Optional.of(Integer.parseInt(s));
        } catch (NumberFormatException exception) {
            return Optional.empty();
        }
    }
}
