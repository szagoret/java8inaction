package java8inaction.ch10;

/**
 *
 * @author szagoret
 */
class Insurance {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
