package java8inaction.ch10;

import java.util.Optional;

/**
 *
 * @author szagoret
 */
class Car {

    private Optional<Insurance> insurance;

    public Optional<Insurance> getInsurance() {
        return insurance;
    }

    void setInsurance(Insurance insurance) {
        this.insurance = Optional.ofNullable(insurance);
    }

}
