package java8inaction.ch10;

import java.util.Optional;

/**
 *
 * @author szagoret
 */
public class Person {

    private Optional<Car> car;

    public Optional<Car> getCar() {
        return car;
    }

    public void setCar(Optional<Car> car) {
        this.car = car;
    }

}
