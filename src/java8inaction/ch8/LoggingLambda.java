package java8inaction.ch8;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author szagoret
 */
public class LoggingLambda {

    public static void main(String[] args) {
        List<Integer> numbers = IntStream.range(1, 100).boxed().collect(Collectors.toList());
        System.out.println("Initial list: " + numbers);
        List<Integer> result = numbers.stream().peek(x -> System.out.println(" from stream: " + x))
                .map(x -> x + 17)
                .peek(x -> System.out.println("after map: " + x))
                .filter(x -> x % 2 == 0)
                .peek(x -> System.out.println("after filter:" + x))
                .limit(3)
                .peek(x -> System.out.println("after limit:" + x))
                .collect(Collectors.toList());

        System.out.println("Result: " + result);
    }
}
