package java8inaction.ch8.designpattern.templatemethod;

/**
 *
 * @author szagoret
 */
class Customer {

    public String getName() {
        return "John";
    }

}
