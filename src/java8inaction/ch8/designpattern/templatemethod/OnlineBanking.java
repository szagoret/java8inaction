package java8inaction.ch8.designpattern.templatemethod;

import java.util.function.Consumer;

/**
 *
 * @author szagoret
 */
public abstract class OnlineBanking {

    public void processCustomer(int id, Consumer<Customer> makeCustomerHappy) {
        Customer c = Database.getCustomerWithId(id);
        makeCustomerHappy.accept(c);
    }
// without lambda we do that
//    public void processCustomer(int id) {
//        Customer c = Database.getCustomerWithId(id);
//        makeCustomerHappy(c);
//    }
//
//    abstract void makeCustomerHappy(Customer c);
}
