package java8inaction.ch8.designpattern.templatemethod;

/**
 *
 * @author szagoret
 */
public class OnlineBankingLambda extends OnlineBanking {

    public static void main(String[] args) {
        new OnlineBankingLambda().processCustomer(1337,
                (Customer c) -> System.out.println("Hello " + c.getName()));
    }
}
