package java8inaction.ch8.designpattern.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * @author szagoret
 */
public class Main {

    final static Map<String, Supplier<Product>> map = new HashMap<>();
    final static Map<String, TriFunction<Integer, Integer, String, Product>> mapWithArgs = new HashMap<>();

    static {
        map.put("loan", Loan::new);
        map.put("stock", Stock::new);
        map.put("bond", Bond::new);
    }

    static {
        mapWithArgs.put("stock", Stock::new);
    }

    public static Product createProduct(String name) {
        Supplier<Product> p = map.get(name);
        if (p != null) {
            return p.get();
        }
        throw new IllegalArgumentException("No souch product " + name);
    }

    public static void main(String[] args) {
        Product p = ProductFactory.createProduct("loan");
        System.out.println("Classic Factory product: " + p);

        /**
         * Using lambda for factory
         */
        Product loanProduct = createProduct("loan");
        System.out.println("Lambda Factory product: " + loanProduct);

        Product stockArgsProduct = mapWithArgs.get("stock").apply(19, 45, "Hello");
        System.out.println("Stock objects with args: " + stockArgsProduct);
    }

}

@FunctionalInterface
interface TriFunction<T, U, V, R> {

    R apply(T t, U u, V v);
}

class ProductFactory {

    public static Product createProduct(String name) {
        switch (name) {
            case "loan":
                return new Loan();
            case "stock":
                return new Stock();
            case "bond":
                return new Bond();
            default:
                throw new RuntimeException("No souch product " + name);
        }
    }
}

class Product {
}

class Loan extends Product {
}

class Stock extends Product {

    private Integer i1;
    private Integer i2;
    private String s1;

    public Stock() {
    }

    public Stock(Integer i1, Integer i2, String s1) {
        this.i1 = i1;
        this.i2 = i2;
        this.s1 = s1;
    }

    @Override
    public String toString() {
        return "Stock [ i1:" + i1 + " i2:" + i2 + " s1:" + s1 + "]";
    }

}

class Bond extends Product {
}
