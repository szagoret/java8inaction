package java8inaction.ch8.designpattern.chainofresponsability;

/**
 *
 * @author szagoret
 */
public abstract class ProcessingObject<T> {

    protected ProcessingObject<T> succesor;

    public void setSuccesor(ProcessingObject<T> succesor) {
        this.succesor = succesor;
    }

    public T handle(T input) {
        T t = handleWork(input);
        if (succesor != null) {
            return succesor.handle(t);
        } else {
            return t;
        }
    }

    abstract protected T handleWork(T input);
}
