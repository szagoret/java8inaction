package java8inaction.ch8.designpattern.chainofresponsability;

/**
 *
 * @author szagoret
 */
public class SpellCheckerProcessing extends ProcessingObject<String> {

    @Override
    protected String handleWork(String input) {
        return input.replaceAll("labda", "lambda");
    }

}
