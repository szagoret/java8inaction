package java8inaction.ch8.designpattern.chainofresponsability;

import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 *
 * @author szagoret
 */
public class Main {

    public static void main(String[] args) {
        ProcessingObject<String> p1 = new HeaderTextProcessing();
        ProcessingObject<String> p2 = new SpellCheckerProcessing();

        p1.setSuccesor(p2);

        String result = p1.handle("Are't labdas really nice?!!");
        System.out.println(result);

        /**
         * using lambda expressions ans UnaryOperators
         */
        // The first processing object
        UnaryOperator<String> headerProcessing = (String text) -> " From Raoul, Mario and Alan: " + text;

        // The second processing object
        UnaryOperator<String> spellCheckerProcessing = (String text) -> text.replace("labda", "lambda");

        // Compose the two functions, resulting in a chain of operations
        Function<String, String> pipeline = headerProcessing.andThen(spellCheckerProcessing);
        String pipelineResult = pipeline.apply("Aren't labdas really nice?!!");
        System.out.println(pipelineResult);

    }
}
