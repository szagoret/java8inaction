package java8inaction.ch8.designpattern.observer;

/**
 *
 * @author szagoret
 */
public class NYTimes implements Observer {

    @Override
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("money")) {
            System.out.println("Breaking news in NY!" + Character.SPACE_SEPARATOR + tweet);
        }
    }

}
