package java8inaction.ch8.designpattern.observer;

/**
 *
 * @author szagoret
 */
public interface Subject {

    void registerObserver(Observer o);

    void notifyObservers(String tweet);
}
