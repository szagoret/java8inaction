package java8inaction.ch8.designpattern.observer;

/**
 *
 * @author szagoret
 */
public class Guardian implements Observer {

    @Override
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("queen")) {
            System.out.println("Yet another news in London..." + tweet);
        }
    }

}
