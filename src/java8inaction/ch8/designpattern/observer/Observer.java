package java8inaction.ch8.designpattern.observer;

/**
 *
 * @author szagoret
 */
public interface Observer {

    void notify(String tweet);
}
