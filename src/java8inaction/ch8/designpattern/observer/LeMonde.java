package java8inaction.ch8.designpattern.observer;

/**
 *
 * @author szagoret
 */
public class LeMonde implements Observer {

    @Override
    public void notify(String tweet) {
        if (tweet != null && tweet.contains("wine")) {
            System.out.println("Today cheese, wine and news!" + Character.SPACE_SEPARATOR + tweet);
        }
    }

}
