package java8inaction.ch8.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author szagoret
 */
public class Feed implements Subject {

    private final List<Observer> observers = new ArrayList<>();

    @Override
    public void registerObserver(Observer o) {
        this.observers.add(o);
    }

    @Override
    public void notifyObservers(String tweet) {
        observers.forEach(o -> o.notify(tweet));
    }

}
