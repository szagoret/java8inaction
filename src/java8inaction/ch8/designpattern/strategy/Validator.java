package java8inaction.ch8.designpattern.strategy;

/**
 *
 * @author szagoret
 */
public class Validator {

    private final ValidationStrategy strategy;

    public Validator(ValidationStrategy v) {
        this.strategy = v;
    }

    public boolean validate(String s) {
        return strategy.execute(s);
    }

    public static void main(String[] args) {
        Validator numericValidator = new Validator(new IsNumeric());
        boolean b1 = numericValidator.validate("aaaa");
        Validator lowerCaseValidator = new Validator(new IsAllLowerCase());
        boolean b2 = lowerCaseValidator.validate("bbbb");

        System.out.println("b1: " + b1 + " b2: " + b2);

        /**
         * Using lambda expressions
         */
        Validator lambdaNumericValidator = new Validator((String s) -> s.matches("[a-z]+"));
        boolean b3 = lambdaNumericValidator.validate("aaaa");
        Validator lambdaLowerCaseValidator = new Validator((String s) -> s.matches("\\d"));
        boolean b4 = lambdaLowerCaseValidator.validate("bbbb");

        System.out.println("lambdaNumericValidator: " + b3
                + " lambdaLowerCaseValidator: " + b4);
    }

}
