package java8inaction.ch8.designpattern.strategy;

/**
 *
 * @author szagoret
 */
@FunctionalInterface
public interface ValidationStrategy {

    boolean execute(String s);
}
