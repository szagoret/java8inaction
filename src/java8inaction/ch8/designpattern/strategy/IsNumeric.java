package java8inaction.ch8.designpattern.strategy;

/**
 *
 * @author szagoret
 */
public class IsNumeric implements ValidationStrategy {

    @Override
    public boolean execute(String s) {
        return s.matches("\\d+");
    }

}
