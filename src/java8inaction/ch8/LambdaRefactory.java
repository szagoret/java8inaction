package java8inaction.ch8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java8inaction.ch2.FilteringApples.Apple;
import java8inaction.ch2.FilteringApples.AppleColor;
import java8inaction.ch4.Dish;
import java8inaction.ch6.CollectingStreams;

/**
 *
 * @author szagoret
 */
public class LambdaRefactory {

    public static void main(String[] args) {
        List<Dish> menu = Arrays.asList(new Dish("pork", false, 800,
                Dish.Type.MEAT), new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT), new Dish(
                        "french fries", true, 530, Dish.Type.OTHER), new Dish(
                        "rice", true, 350, Dish.Type.OTHER), new Dish(
                        "season fruit", true, 120, Dish.Type.OTHER), new Dish(
                        "pizza", true, 550, Dish.Type.OTHER), new Dish(
                        "prawns", false, 300, Dish.Type.FISH), new Dish(
                        "salmon", false, 450, Dish.Type.FISH));

        // pass anonymous class
        doSomething(new Task() {
            @Override
            public void execute() {
                System.out.println("Danger danger!");
            }
        });

        // But converting this anonymous class to a lambda expressio
        // call, because both Runnable and Task are valid target types:
        // doSomething(() -> System.out.println(" Danger Danger!!"););
        // You can solve the ambiguity by providing an explicit cast (Task):
        doSomething((Task) () -> System.out.println("Danger danger!!"));

        /**
         * From lambda expressions to method references
         */
        Map<CollectingStreams.CaloricLevel, List<Dish>> dishByCaloricLevel
                = menu.stream().collect(Collectors.groupingBy(dish -> {
                    if (dish.getCalories() <= 400) {
                        return CollectingStreams.CaloricLevel.DIET;
                    } else if (dish.getCalories() <= 700) {
                        return CollectingStreams.CaloricLevel.NORMAL;
                    } else {
                        return CollectingStreams.CaloricLevel.FAT;
                    }
                }));
        // You can extract the lambda expression into a separate method and pass it as argument to
        // groupingBy. The code becomes more concise and its intent is now more explicit:
        Map<CollectingStreams.CaloricLevel, List<Dish>> dishesByCaloricLevel1 = menu.stream()
                .collect(Collectors.groupingBy(Dish::getCaloricLevel));
        System.out.println("Dish by caloric level: " + dishesByCaloricLevel1);

        List<Apple> inventory = Arrays.asList(new Apple(20, AppleColor.red),
                new Apple(20, AppleColor.blue),
                new Apple(200, AppleColor.green),
                new Apple(155, AppleColor.red), new Apple(120, "green"));
        List<Apple> inventoryCopy = inventory.subList(0, inventory.size());

        // You need to think about the implementation
        inventory.sort((Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight()));
        // Reads like a problem statement
        inventory.sort(Comparator.comparing(Apple::getWeight));
        System.out.println("");
    }

    public static void doSomething(Runnable r) {
        r.run();
    }

    public static void doSomething(Task t) {
        t.execute();
    }
}

interface Task {

    public void execute();
}
